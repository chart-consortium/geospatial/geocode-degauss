# Geocode Degauss Singularity

A repo to build a container for running [Degauss](https://degauss.org/index.html) to geocode addresses. The container is bootstrapped from the ['geocoder' image](https://degauss.org/geocoder/), can run RStudio Server and has additional tools including tidyverse and the 'sf' R package.

## Building

- A new container build will only be initiated by a modification to the Singularity.def file
- The built container file will only be pushed to the Duke OIT registry when a commit is tagged (ideally following the current version naming convention)
    + Note that to push, the container must be re-built first.

## Pulling

To pull the latest version of the container file via Singularity, use the following command where "/path/to/container/directory" is the directory where the container will be pulled to:

```
singularity pull --force --dir /path/to/container/directory oras://gitlab-registry.oit.duke.edu/chart-consortium/geospatial/geocode-degauss/geocode-degauss:latest
```

Alternatively, the latest container version can be pulled using wget (to the current directory by default):

```
wget --no-check-certificate https://research-singularity-registry-public.oit.duke.edu/chart-consortium/geocode-degauss.sif
```

## Running

The container can be run using something like the following, where "path to address CSV" is the path to CSV file with addresses:

```
export ADDRESS_FILE="path to address CSV"
singularity run geocode-degauss.sif ${ADDRESS_FILE}
```

## Notes


